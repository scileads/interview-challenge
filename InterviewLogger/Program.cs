﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace InterviewLogger
{

    //QUESTIONS:
    //1) Can you give a brief overview / summary of what it is this code is doing?
    //2) What happens if we call Logger.Log? Why?
    //3) What is protected, private, internal, public?
    //4) What is abstract? Why would you use it?
    //5) What is a constructor?
    //6) What happens if the file that the text writer is trying to access is locked?
    //7) What are two uses for the 'using' statement in c sharp
    //8) Do you know why you would want to use the async / await pattern?  Explain the differences between parallel and async
    //9) Do you think you could modify this code in order to make it build? Or have a stab at something (there is no right or wrong answer - just whatever it takes to make it work)
    //10) For extra bonus points, could you pick a library to parse the HTML result returned from the ScraperLogger class and write a news headline to a textfile? Optional extra 


    class Program
    {
        static void Main(string[] args)
        {
            RunWithHelloWorld().Wait();
        }

        public static async Task RunWithHelloWorld()
        {
            string[] args = { "hello", "world" };

            var loggers = new List<LoggerBase>();

            loggers.Add(new ConsoleLogger());
            loggers.Add(new FileLogger("mylog.txt"));


            foreach (var logger in loggers)
            {
                logger.LogAll(args);
            }

            var webLogger = new ScraperLogger("webLog.txt");
            webLogger.Log("http://www.bbc.co.uk/news");
        }
    }

    public abstract class LoggerBase
    {
        public void LogAll(string[] messages)
        {
            foreach (var message in messages)
            {
                Log(message);
            }
        }

        protected abstract void Log(string message);
    }

    internal class ConsoleLogger : LoggerBase
    {
        protected override void Log(string message)
        {
            Console.WriteLine(message);
        }
    }

    internal class FileLogger : LoggerBase
    {
        private string _logFilePath;

        public FileLogger(string logFilePath)
        {
            _logFilePath = logFilePath;
        }

        protected override void Log(string message)
        {
            File.WriteAllText(_logFilePath, message);
        }
    }

    internal class ScraperLogger : LoggerBase
    {
        private readonly string _logFilePath;

        public ScraperLogger(string logFilePath)
        {
            _logFilePath = logFilePath;
        }

        static async Task WriteTextAsync(string filePath, string text)
        {
            byte[] encodedText = Encoding.Unicode.GetBytes(text);

            using (FileStream sourceStream = new FileStream(filePath,
                FileMode.Append, FileAccess.Write, FileShare.None,
                bufferSize: 4096, useAsync: true))
            {
                await sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
            };
        }

        protected override async void Log(string message)
        {
            var webClient = new WebClient();
            try
            {
                var html = webClient.DownloadStringTaskAsync(new Uri(message)).Result;
                await WriteTextAsync(_logFilePath, html);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }



    }
}
