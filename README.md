**QUESTIONS:**

1) Can you give a brief overview / summary of what it is this code is doing?

2) What happens if we call Logger.Log? Why?

3) What is protected, private, internal, public?

4) What is abstract? Why would you use it?

5) What is a constructor?

6) What happens if the file that the text writer is trying to access is locked?

7) What are two uses for the 'using' statement in c sharp

8) Do you know why you would want to use the async / await pattern? Explain the differences between parallel and async

9) Do you think you could modify this code in order to make it build? Or have a stab at something (there is no right or 
wrong answer - just whatever it takes to make it work)
   
10) For extra bonus points, could you pick a library to parse the HTML result returned from the ScraperLogger class and 
write a news headline to a textfile? Optional extra
